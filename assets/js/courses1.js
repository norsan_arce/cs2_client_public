let access = localStorage.getItem('token');
let adminButton = document.querySelector("#adminButton")
let userButton = document.querySelector("#userButton")
console.log(access)




    // admin/User buttons
if(access){
    fetch(`http://localhost:4000/api/users/details`, {
        headers: {
            Authorization: `Bearer ${access}`
        }
     })
     .then(res => res.json())
     .then(data => {
     
    if(data.isAdmin === true){
                    adminButton.innerHTML = `<a href="#" class="nav-link" id="#adminButton"></a>`
                    
                    
    const get = fetch(`http://localhost:4000/api/courses/all`, {
        headers: {
        Authorization: `Bearer ${access}`
        }

    })


        get.then(res => res.json())
        .then(data => {

    let courses;
    for(let i = 0; i< data.length; i++){
        console.log(data[i]._id)
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButton">
                        <a href="editCourse.html?id=${(data[i]._id)}" class="btn btn-sm btn-primary onclick">
                                Edit
                        </a>
                        <a href="deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
                                Delete
                        </a>
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Course Details</a>
                    </div>
                    <div class="card-footer" id="userButton">
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input" data-toggle="toggle" data-on="Active" data-off="Archived" id="customSwitch1" data-onstyle="light" data-size="mini" data-offstyle="dark" checked>

                          <label class="custom-control-label" for="customSwitch1"></label>
                        </div>                        
                    </div>

                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButton">
                        <a href="./editCourse.html?id=${data[i]._id}" class="btn btn-sm btn-primary onclick">
                                Edit
                        </a>
                        <a href="./deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
                                Delete
                        </a>
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Course Details</a>
                    </div>
                    <div class="card-footer" id="userButton">
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input" data-toggle="toggle" data-on="Active" data-off="Archived" id="customSwitch1" data-onstyle="light" data-size="mini" data-offstyle="dark" checked>

                          <label class="custom-control-label" for="customSwitch1"></label>
                        </div> 
                    </div>

                </div>
            </div>`
        }

        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    } 
    
        let toggle =  document.querySelector("#customSwitch1")


        let parameter = window.location.search;
        const urlId = new URLSearchParams(parameter);
        let id = urlId.get('id');

        toggle.addEventListener('toggle', (e) =>{
            
             e.preventDefault()


             if(data.isActive === true){
                fetch(`http://localhost:4000/api/courses/${id}`, {
                    method: 'PUT',
                    headers: {
                        Authorization: `Bearer ${userToken}`
                    }
                })
             }else{
                
             }
        })
    

 })
}else {
    const get = fetch(`http://localhost:4000/api/courses/all`)


         get.then(res => res.json())
         .then(data => {

    let courses;
    for(let i = 0; i< data.length; i++){
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButton">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}"  class="btn btn-sm btn-light">
                            Enroll
                        </a>
                    </div>
                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButton">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}" class="btn btn-sm btn-light">
                            Enroll
                        </a>
                    </div>
                </div>
            </div>`
        }
        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    } 
    
})

}
})
}
