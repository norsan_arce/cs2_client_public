let createCourseForm = document.querySelector("#createCourse")
let token = localStorage.getItem("token")

createCourseForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value;
	let coursePrice = document.querySelector("#coursePrice").value
	let courseDescription = document.querySelector("#courseDescription").value


console.log(courseName)
console.log(coursePrice)
console.log(courseDescription)	

	if(courseName !== '' && coursePrice !== '' && courseDescription !== ''){
		fetch('http://localhost:4000/api/courses',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: courseDescription,
				price: coursePrice
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			console.log(data)
			if (data === true) {
						alert('Successfully Added New Course');
						window.location.replace('./courses.html')
					}else{
						alert('Adding new course failed');
					}
		})
	}
})