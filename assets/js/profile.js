let token = localStorage.getItem("token")

console.log(token)


let profileContainer = document.querySelector('#profileContainer')

if(!token || token === null) {
	window.location.href="./login.html"
	alert("You must login first")
} else {
	fetch('https://radiant-earth-12979.herokuapp.com/api/users/details',{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data =>  {
		if(data.isAdmin === true) {
			window.location.href="./courses.html"
		}
		console.log(data)
		let enrollmentData = data.enrollments.map(classData =>{
			return `<tr>
					<td>${classData.courseId}</td> 
					<td>${classData.enrolledOn}</td> 
					<td>${classData.status}</td> 
					</tr> `
		console.log(classData)
		}).join('')
		profileContainer.innerHTML = `<div class="col-md-12">
				<section class="jumbotron bg-white my-5">
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<h3 class="text-center">Class History</h3>
					<table class="table">
						<thead>
							<tr>
								<th> CourseID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
							<tbody>
								${enrollmentData}
							</tbody>
						</thead>
					</table>
				</section>
			</div>
		`
	})




}


