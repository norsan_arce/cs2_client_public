// console.log("hello from register");


let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let firstName= document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let email = document.querySelector("#userEmail").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

console.log(firstName);
console.log(lastName);
console.log(email);
console.log(mobileNo);
console.log(password1);
console.log(password2);

	//lets create a validation to enable the submit button when all fields are registered & if both passwords match
	if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {

	//if all the requirements are met. then lets check for duplicate email addresses
		fetch('https://radiant-earth-12979.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ //this is a method that converts a javascript value into a JSON string
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			//We will now create a control strucutre to see if there are no duplicates found
			if(data === false){
				fetch('https://radiant-earth-12979.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => {
					return res.json();
				})
				.then(data => {
					console.log(data);
					//lets give a proper response if registration was successful
					if (data === true) {
						alert('Registration successful');
						//after confirmation of alert window, lets direct the user to he login page
						window.location.replace('./login.html')
					}else{
						alert('Registration failed');
					}
				})
			}
		})
	}else{
		alert("Something went wrong, please try again");
	}
})
