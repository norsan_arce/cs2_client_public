let access = localStorage.getItem("token")
let adminButton = document.querySelector("#adminButton")
let profile = document.querySelector("#profile")
let adminButtons = document.querySelector("#adminButtons")
let coursesContainer = document.querySelector('#courseContainer')
let createCourse = document.querySelector('#createCourse');
console.log(access)

//Admin/User

if(access){
    fetch(`http://localhost:4000/api/users/details`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access}`
        }
    })
    .then(res => res.json())
    .then(data => {
        if(data.isAdmin === true){
            adminButton.innerHTML = `<li class="nav-item mr-3" id="adminButton">
            <a href="./courses.html" class="nav-link">Admin</a>
            </li>`
            profile.innerHTML = `<li class="nav-item">
            <a href="./courses.html" class="nav-link">Courses</a>
            </li>`
            createCourse.innerHTML = `<a class="btn btn-primary" id="createCourse" href="./addCourse.html" role="button">Create</a>`

        }


    }).then(data =>{
        fetch('http://localhost:4000/api/courses',{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${access}`
            }
        }).then(res => res.json())
           .then(data => {
            // console.log(data)

            let courses;
    for(let i = 0; i< data.length; i++){
        console.log(data[i]._id)
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header">
         
          
                    </div>
                    <div class="card-body">
                        <h3 class="card-title"> ${data[i].name}</h3>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Enroll</a>
                    </div>
                    <div class="card-footer" id="adminButtons">
                                 <a href="editCourse.html?id=${(data[i]._id)}" class="btn btn-sm btn-primary onclick">
                                Edit
                        </a>
                        <a href="deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
                                Delete
                        </a>            
                    </div>

                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header">
                   
                    </div>
                    <div class="card-body justify-content-center text-align-center">
                        <h3 class="card-title"> ${data[i].name}</h3>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Enroll</a>
                    </div>
                    <div class="card-footer justify-content-center" id="adminButtons">
                             <a href="./editCourse.html?id=${data[i]._id}" class="btn btn-sm btn-primary onclick">
                                Edit
                        </a>
                        <a href="./deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
                                Delete
                        </a>
                    </div>

                </div>
            </div>`
        }
        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        } 
    }


    })

           })
    // })

    fetch('http://localhost:4000/api/courses',{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${access}`
            }
        }).then(res => res.json())
           .then(data => {
            // console.log(data)



    let courses;
    for(let i = 0; i< data.length; i++){
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}"  class="btn btn-sm btn-light">
                            Enroll
                        </a>
                    </div>
                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}" class="btn btn-sm btn-light">
                            Enroll
                        </a>
                    </div>
                </div>
            </div>`
        }
        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    } 
    
})

}else{

    profile.innerHTML = `<li class="nav-item">
    <a href="./register.html" class="nav-link">Register</a>
    </li>`

fetch('http://localhost:4000/api/courses',{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${access}`
            }
        }).then(res => res.json())
           .then(data => {
            // console.log(data)



    let courses;
    for(let i = 0; i< data.length; i++){
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}"  class="btn btn-sm btn-light">
                            Enroll
                        </a>
                    </div>
                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}" class="btn btn-sm btn-light">
                            Enroll
                        </a>
                    </div>
                </div>
            </div>`
        }
        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    } 
    
})

}
