let token = localStorage.getItem("token")
let adminButtons = document.querySelector("#adminButton")
// let userButton = document.querySelector("#userButton")
let coursesContainer = document.querySelector('#courseContainer')
console.log(token)


if(!token || token === null) {
	window.location.href="./login.html"
	alert("You must login first")
} else {
	fetch('http://localhost:4000/api/courses',{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data =>  {
		console.log(data)

		//TEST CODE
		// for (j = 0; j < data.length; j++){
		// 	// let x2 = (data[j].name + " " + data[j].description + " " +data[j].price)
		// 	// let output1 = document.querySelector('#courseName').innerHTML = `${data[j].name}`;
		// 	// let output2 = document.querySelector('#coursePrice').innerHTML = data[j].price;
		// 	// let output3 = document.querySelector('#courseDescription').innerHTML = data[j].description;

		// 	// let output4 = output1 + output2 + output3;

		// 	let displayCourse = document.createElement('p');
		// 	displayCourse.innerText = `Course Name: ${data[j].name} 
		// 	Course Price: ${data[j].price} 
		// 	Course Description: ${data[j].description}`;
		// 	document.querySelector('#courseContainer').appendChild(displayCourse);

		// }

 let courses;
    for(let i = 0; i< data.length; i++){
        console.log(data[i]._id)
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                        <a href="editCourse.html?id=${(data[i]._id)}" class="btn btn-sm btn-primary onclick">
                                Edit
                        </a>
                        <a href="deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
                                Delete
                        </a>
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Enroll</a>
                    </div>
                    <div class="card-footer" id="userButton">
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input" data-toggle="toggle" data-on="Active" data-off="Archived" id="customSwitch1" data-onstyle="light" data-size="mini" data-offstyle="dark" checked>

                          <label class="custom-control-label" for="customSwitch1"></label>
                        </div>                        
                    </div>

                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                        <a href="./editCourse.html?id=${data[i]._id}" class="btn btn-sm btn-primary onclick">
                                Edit
                        </a>
                        <a href="./deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
                                Delete
                        </a>
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Enroll</a>
                    </div>
                    <div class="card-footer" id="userButton">
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input" data-toggle="toggle" data-on="Active" data-off="Archived" id="customSwitch1" data-onstyle="light" data-size="mini" data-offstyle="dark" checked>

                          <label class="custom-control-label" for="customSwitch1"></label>
                        </div> 
                    </div>

                </div>
            </div>`
        }
        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    }


	})





}

// 		for (j = 0; j < data.length; j++){
// 			// let x2 = (data[j].name + " " + data[j].description + " " +data[j].price)
// 			// let output1 = document.querySelector('#courseName').innerHTML = `${data[j].name}`;
// 			// let output2 = document.querySelector('#coursePrice').innerHTML = data[j].price;
// 			// let output3 = document.querySelector('#courseDescription').innerHTML = data[j].description;

// 			// let output4 = output1 + output2 + output3;

// 			let displayCourse = document.createElement('p');
// 			displayCourse.innerText = `Course Name: ${data[j].name} 
// 			Course Price: ${data[j].price} 
// 			Course Description: ${data[j].description}`;
// 			document.querySelector('#courseContainer').appendChild(displayCourse);


			// courseContainer.innerHTML = displayCourse;

			// let x =  `<div class="card-body">
			// 		<h5 class="card-title text-primary" id="course">Learn ${data[j].name}</h5>
			// 		<h6 class="card-title text-dark">For Only ${data[j].price}</h6>
			// 		<p class="card-text text-dark">${data[j].description}</p>
			// 	</div>`
			// coursesContainer.innerHTML = x;
				// for()
			// let displayContent.innerText = x;
			// document.querySelector('#displayCourse').appendChild(displayContent);
// }		

		// 		courseContainer.innerHTML = `<div class="row row-cols-1 row-cols-md-3 mt-md-5">
		// <div class="col mb-4">
		// 	<div class="card h-100">
		// 		<img src="https://via.placeholder.com/300x300" class="card-img-top" alt="300x300">
		// 		<div class="card-body">
		// 			<h5 class="card-title text-primary" id="course">Learn ${data[j].name}</h5>
		// 			<h6 class="card-title text-dark">For Only ${data[j].price}</h6>
		// 			<p class="card-text text-dark">${data[j].description}</p>
		// 		</div>
		// 		<div class="card-footer">
		// 			<button type="submit" class="btn btn-primary my-3">View</button>
		// 			<button type="submit" class="btn btn-primary my-3">Edit</button>
		// 			<button type="submit" class="btn btn-danger my-3">Delete</button>
		// 		</div>
		// 	</div>
		// </div>`
		
		//   let displayArray = document.createElement('li');
  //           displayArray.innerText = content;
  //           document.querySelector('#arrayContent').appendChild(displayArray);


	// 		courseContainer.innerHTML = `<div class="row row-cols-1 row-cols-md-3 mt-md-5">
	// 	<div class="col mb-4">
	// 		<div class="card h-100">
	// 			<img src="https://via.placeholder.com/300x300" class="card-img-top" alt="300x300">
	// 			<div class="card-body">
	// 				<h5 class="card-title text-primary" id="course">Learn ${data[j].name}</h5>
	// 				<h6 class="card-title text-dark">For Only ${data[j].price}</h6>
	// 				<p class="card-text text-dark">${data[j].description}</p>
	// 			</div>
	// 			<div class="card-footer">
	// 				<button type="submit" class="btn btn-primary my-3">View</button>
	// 				<button type="submit" class="btn btn-primary my-3">Edit</button>
	// 				<button type="submit" class="btn btn-danger my-3">Delete</button>
	// 			</div>
	// 		</div>
	// 	</div>
	// 	<div class="col mb-4">
	// 		<div class="card h-100">
	// 			<img src="https://via.placeholder.com/300x300" class="card-img-top" alt="300x300">
	// 			<div class="card-body">
	// 				<h5 class="card-title text-primary">Learn ${data[0].name}</h5>
	// 				<h6 class="card-title text-dark">For Only ${data[0].price}</h6>
	// 				<p class="card-text text-dark">${data[0].description}</p>
	// 			</div>
	// 			<div class="card-footer">
	// 				<button type="submit" class="btn btn-primary my-3">View</button>
	// 				<button type="submit" class="btn btn-primary my-3">Edit</button>
	// 				<button type="submit" class="btn btn-danger my-3">Delete</button>
	// 			</div>
	// 		</div>
	// 	</div>
	// 	<div class="col mb-4">
	// 		<div class="card h-100">
	// 			<img src="https://via.placeholder.com/300x300" class="card-img-top" alt="300x300">
	// 			<div class="card-body">
	// 				<h5 class="card-title text-primary">Learn ${data[2].name}</h5>
	// 				<h6 class="card-title text-dark">For Only ${data[2].price}</h6>
	// 				<p class="card-text text-dark">${data[2].description}</p>
	// 			</div>
	// 			<div class="card-footer">
	// 				<button type="submit" class="btn btn-primary my-3">View</button>
	// 				<button type="submit" class="btn btn-primary my-3">Edit</button>
	// 				<button type="submit" class="btn btn-danger my-3">Delete</button>
	// 			</div>
	// 		</div>
	// 	</div>
	// </div>

	// 	`
	// 	}

	// })

// let contents_array = data;
//         let lastEntry = contents_array[contents_array.length -1];
//             let lastItem = document.createElement('li');
//             lastItem.innerText = lastEntry;
//             document.querySelector('#lastItem').append(lastItem); 

// let addedInput = document.createElement('p');
// addedInput.innerText = 

//    let addedInput = document.createElement('li');
//             addedInput.innerText = lastEntry;
//             document.querySelector('#arrayContent').append(addedInput);

// }


	// for (var j = 0; j < myArray.length; j++){
	// 	console.log(myArray[j].x);
	// }





// let token = localStorage.getItem('token');
// let adminButtons = document.querySelector("#adminButton")
// let userButton = document.querySelector("#userButton")
// console.log(token)


//     // admin/User buttons
// if(token){
//     fetch(`http://localhost:4000/api/users/details`, {
//         headers: {
//             Authorization: `Bearer ${token}`
//         }
//      })
//      .then(res => res.json())
//      .then(data => {
     
//     if(data.isAdmin === true){
//                     adminButtons.innerHTML = 
//                     `<div class="card jumbotron col-md-4 mx-auto">
//                         <h3 class="text-center"> Admin Buttons </h3>
//                         <div class="col-md-12">
//                             <div class="card">
//                                 <a href="./addCourse.html" class="btn btn-sm btn-success">
//                                 Add a Course
//                                 </a>
//                             </div>
//                         </div>
//                     </div>
//                     `
//     const get = fetch(`http://localhost:4000/api/courses/all`, {
//         headers: {
//         Authorization: `Bearer ${token}`
//         }

//     })


//         get.then(res => res.json())
//         .then(data => {

//     let courses;
//     for(let i = 0; i< data.length; i++){
//         console.log(data[i]._id)
//         if(courses){
//             courses = `${courses}
//             <div class="col-md-4 my-3">
//                 <div class="card">
//                     <div class="card-header" id="adminButtons">
//                         <a href="editCourse.html?id=${(data[i]._id)}" class="btn btn-sm btn-primary onclick">
//                                 Edit
//                         </a>
//                         <a href="deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
//                                 Delete
//                         </a>
//                     </div>
//                     <div class="card-body">
//                         <h2 class="card-title"> ${data[i].name}</h2>
//                         <p class="card-title"> ${data[i].description}</p>
//                         <p class="card-text">Price: ${data[i].price}</p>
//                         <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Course Details</a>
//                     </div>
//                     <div class="card-footer" id="userButton">
//                         <div class="custom-control custom-switch">
//                           <input type="checkbox" class="custom-control-input" data-toggle="toggle" data-on="Active" data-off="Archived" id="customSwitch1" data-onstyle="light" data-size="mini" data-offstyle="dark" checked>

//                           <label class="custom-control-label" for="customSwitch1"></label>
//                         </div>                        
//                     </div>

//                 </div>
//             </div>`
//         }else{
//             courses = `<div class="col-md-4 my-3">
//                 <div class="card">
//                     <div class="card-header" id="adminButtons">
//                         <a href="./editCourse.html?id=${data[i]._id}" class="btn btn-sm btn-primary onclick">
//                                 Edit
//                         </a>
//                         <a href="./deleteCourse.html?id=${data[i]._id}" class="btn btn-sm btn-danger onclick">
//                                 Delete
//                         </a>
//                     </div>
//                     <div class="card-body">
//                         <h2 class="card-title"> ${data[i].name}</h2>
//                         <p class="card-title"> ${data[i].description}</p>
//                         <p class="card-text">Price: ${data[i].price}</p>
//                         <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Course Details</a>
//                     </div>
//                     <div class="card-footer" id="userButton">
//                         <div class="custom-control custom-switch">
//                           <input type="checkbox" class="custom-control-input" data-toggle="toggle" data-on="Active" data-off="Archived" id="customSwitch1" data-onstyle="light" data-size="mini" data-offstyle="dark" checked>

//                           <label class="custom-control-label" for="customSwitch1"></label>
//                         </div> 
//                     </div>

//                 </div>
//             </div>`
//         }

//         if(courses){
//             document.querySelector("#courseContainer").innerHTML = courses;
//         }
//     } 
    
//         let toggle =  document.querySelector("#customSwitch1")


//         let parameter = window.location.search;
//         const urlId = new URLSearchParams(parameter);
//         let id = urlId.get('id');

//         toggle.addEventListener('toggle', (e) =>{
            
//              e.preventDefault()


//              if(data.isActive === true){
//                 fetch(`http://localhost:4000/api/courses/${id}`, {
//                     method: 'PUT',
//                     headers: {
//                         Authorization: `Bearer ${userToken}`
//                     }
//                 })
//              }else{
                
//              }
//         })
    

//  })
// }else {
//     const get = fetch(`http://localhost:4000/api/courses/all`)


//          get.then(res => res.json())
//          .then(data => {

//     let courses;
//     for(let i = 0; i< data.length; i++){
//         if(courses){
//             courses = `${courses}
//             <div class="col-md-4 my-3">
//                 <div class="card">
//                     <div class="card-header" id="adminButtons">
//                     </div>
//                     <div class="card-body">
//                         <h2 class="card-title"> ${data[i].name}</h2>
//                         <p class="card-title"> ${data[i].description}</p>
//                         <p class="card-text">Price: ${data[i].price}</p>
//                     </div>
//                     <div class="card-footer" id="userButton">
//                         <a href="./course.html?id=${data[i]._id}"  class="btn btn-sm btn-light">
//                             Enroll
//                         </a>
//                     </div>
//                 </div>
//             </div>`
//         }else{
//             courses = `<div class="col-md-4 my-3">
//                 <div class="card">
//                     <div class="card-header" id="adminButtons">
//                     </div>
//                     <div class="card-body">
//                         <h2 class="card-title"> ${data[i].name}</h2>
//                         <p class="card-title"> ${data[i].description}</p>
//                         <p class="card-text">Price: ${data[i].price}</p>
//                     </div>
//                     <div class="card-footer" id="userButton">
//                         <a href="./course.html?id=${data[i]._id}" class="btn btn-sm btn-light">
//                             Enroll
//                         </a>
//                     </div>
//                 </div>
//             </div>`
//         }
//         if(courses){
//             document.querySelector("#courseContainer").innerHTML = courses;
//         }
//     } 
    
// })

// }
// })
// }
