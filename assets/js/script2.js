  
let navItems =  document.querySelector("#navSession");

// console.log(navItems);

let userToken = localStorage.getItem("token")
//to check if successful:
// console.log(userToken)


if(!userToken) {
	navItems.innerHTML = 
	`
	<li class="nav-item">
	<a href="./pages/login.html" class="nav-link"> Log in </a>
	</li>

	`
} else {
	navItems.innerHTML = `
	<li class="nav-item">
	<a href="./pages/logout.html" class="nav-link"> Log Out </a>
	</li>
	`
}

