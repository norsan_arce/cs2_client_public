let access = localStorage.getItem('token');
let deleteCourse= document.querySelector("#deleteCourse");

let full_url = document.URL; // Get current url
let url_array = full_url.split('=') // Split the string into an array with / as separator
let last_segment = url_array[url_array.length-1];  // Get the last part of the array (-1)
console.log(`Thrown url ID: ${last_segment}`);




deleteCourse.addEventListener("click", (e) => {
    e.preventDefault()


    if(access){
        fetch(`https://radiant-earth-12979.herokuapp.com/api/courses/${last_segment}`, {
            method:'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${access}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            // let courseId = data._id;
            // console.log(courseId);
            if (data === true) {
                alert('Successfully Deleted Course');
                window.location.replace('./courses.html')
            }
        })
        
    }

})